package edu.mosense;

import java.util.ArrayList;

public class PearsonCorrelationCoefficient {
	private ArrayList<AccelData> sensorData;
	private double[][] relationValue = new double[3][3];
	private double xAverage, yAverage, zAverage;

	public PearsonCorrelationCoefficient(ArrayList<AccelData> sensorData) {
		relationValue[0][0] = 1.000d;
		relationValue[1][1] = 1.000d;
		relationValue[2][2] = 1.000d;
		this.sensorData = sensorData;
		CalculateXYZAverage();
		XYRelationValue();
		XZRelationValue();
		YZRelationValue();
	}

	private void CalculateXYZAverage() {
		double[] sum = new double[3];
		for (int i = 0; i < sensorData.size(); i++) {
			sum[0] = sum[0] + sensorData.get(i).getX();
			sum[1] = sum[1] + sensorData.get(i).getY();
			sum[2] = sum[2] + sensorData.get(i).getZ();
		}
		xAverage = sum[0] / sensorData.size();
		yAverage = sum[1] / sensorData.size();
		zAverage = sum[2] / sensorData.size();
	}

	private void XYRelationValue() {
		
		double Sxx = 0,Syy=0,Sxy = 0;
		for (int i = 0; i < sensorData.size(); i++) {
			Sxx  = Sxx + Math.pow(sensorData.get(i).getX()-xAverage, 2);
			Syy = Syy + Math.pow(sensorData.get(i).getY()-yAverage, 2);
			Sxy = Sxy +(sensorData.get(i).getX()-xAverage)*(sensorData.get(i).getY()-yAverage);
		}
		Sxx = Math.sqrt(Sxx);
		Syy = Math.sqrt(Syy);
		relationValue[0][1] = Sxy/(Sxx*Syy);
		relationValue[1][0] = relationValue[0][1];
	}

	private void XZRelationValue() {
		double Sxx = 0,Szz=0,Sxz = 0;
		for (int i = 0; i < sensorData.size(); i++) {
			Sxx  = Sxx + Math.pow(sensorData.get(i).getX()-xAverage, 2);
			Szz = Szz + Math.pow(sensorData.get(i).getZ()-zAverage, 2);
			Sxz = Sxz +(sensorData.get(i).getX()-xAverage)*(sensorData.get(i).getZ()-zAverage);
		}
		Sxx = Math.sqrt(Sxx);
		Szz = Math.sqrt(Szz);
		relationValue[0][2] = Sxz/(Sxx*Szz);
		relationValue[2][0]=relationValue[0][2];
	}
	

	private void YZRelationValue() {
		double Syy = 0,Szz=0,Syz = 0;
		for (int i = 0; i < sensorData.size(); i++) {
			Syy  = Syy + Math.pow(sensorData.get(i).getY()-yAverage, 2);
			Szz = Szz + Math.pow(sensorData.get(i).getZ()-zAverage, 2);
			Syz = Syz +(sensorData.get(i).getY()-yAverage)*(sensorData.get(i).getZ()-zAverage);
		}
		Syy = Math.sqrt(Syy);
		Szz = Math.sqrt(Szz);
		relationValue[1][2] = Syz/(Syy*Szz);
		relationValue[2][1]=relationValue[1][2];
	}
	
//	private int findRelationAxi(){
//		double[] relationXYZValue ={relationValue[0][1],relationValue[0][2],relationValue[1][2]};
//		double max = Math.abs(relationXYZValue[i]);
//		for (int i = 0; i < relationXYZValue.length; i++) {
//			
//		}
//		return 1;
//	}
	public double[][] getRelationValue() {
		return relationValue;
	}

	
}
