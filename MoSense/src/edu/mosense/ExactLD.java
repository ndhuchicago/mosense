package edu.mosense;

public class ExactLD {
	private int i, j;
	private int n, m;
	private int cost = 0;
	private int[][] LD;
	private double exactCost; //�s��Z��
	private String X, Y;

	public ExactLD(String str1, String str2) {
		this.X = str1;
		this.Y = str2;
		this.n = str1.length();
		this.m = str2.length();
		this.LD = new int[n + 1][m + 1];
		exactCost = exactLevenshtein();
	}

	private int exactLevenshtein() {

		for (i = 0; i <= n; i++) {
			LD[i][0] = i;
		}

		for (j = 0; j <= m; j++) {
			LD[0][j] = j;
		}

		for (i = 1; i <= n; i++) {
			for (j = 1; j <= m; j++) {

				if (Math.abs((int) X.charAt(i - 1)- (int) Y.charAt(j - 1)) <= 0) {
					cost = 0;
				} else {
					cost = 1;
				}

				LD[i][j] = findMinIntValue(LD[i - 1][j] + 1, LD[i][j - 1] + 1,
						LD[i - 1][j - 1] + cost);
			}
		}

		return LD[n][m];
	}
	
	  private  int findMinIntValue(int a, int b, int c) {
	        int min = a;
	        if (b < a) {
	            min = b;
	        } else if (c < min) {
	            min = c;
	        }
	        return min;
	    }

	public double getExactCost() {
		return exactCost;
	}
	  
	  
}
