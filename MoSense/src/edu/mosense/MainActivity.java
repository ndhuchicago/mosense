package edu.mosense;

import java.util.ArrayList;
import edu.mosense.service.AccelDetectService;
import edu.mosense.service.GPSService;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private Button training_Data_Collectbtn;
	private Button fall_Detectionbtn;
	private Button accel_detect_starbtn;
	private Button accel_detect_stopbtn;
	private Button audio_settingbtn;
	private Button callphone_settingbtn;
	private Button sms_settingbtn;
	private Button startGPS;
	private TextView accel_text;
	public ArrayList<AccelData> sensorData;
	private boolean isBound = false;

	private void checkGPSOption() {
		Intent  intent = new Intent(this,GPSService.class);
		LocationManager localLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		if (localLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			startService(intent);
		}else{
			stopService(intent);
		}
		return;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/** 元件初始化 */
		training_Data_Collectbtn = (Button) findViewById(R.id.Training_Data_Collectbtn);
		fall_Detectionbtn = (Button) findViewById(R.id.fall_Detectionbtn);
		accel_detect_starbtn = (Button) findViewById(R.id.Accel_detect_service_starbtn);
		accel_detect_stopbtn = (Button) findViewById(R.id.Accel_detect_service_stopbtn);
		audio_settingbtn = (Button) findViewById(R.id.PreAlart);
		callphone_settingbtn = (Button) findViewById(R.id.PhoneCall);
		startGPS = (Button) findViewById(R.id.GPS);
		sms_settingbtn = (Button) findViewById(R.id.SMS);
		accel_text = (TextView) findViewById(R.id.load_value);

		/** 設定監聽器 */
		training_Data_Collectbtn
				.setOnClickListener(training_data_collect_listener);
		fall_Detectionbtn.setOnClickListener(fall_detection_listener);
		audio_settingbtn.setOnClickListener(audio_setting_listener);
		callphone_settingbtn.setOnClickListener(callphone_setting_listener);
		sms_settingbtn.setOnClickListener(sms_setting_listener);
		startGPS.setOnClickListener(startgps_listener);
		accel_detect_starbtn
				.setOnClickListener(laccel_detect_servicestar_listener);
		accel_detect_stopbtn
				.setOnClickListener(laccel_detect_servicestop_listener);
		accel_detect_stopbtn.setEnabled(isBound);

	}

	/** 監聽事件 */
	OnClickListener laccel_detect_servicestar_listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(MainActivity.this,
					edu.mosense.service.AccelDetectService.class);
			if (!isBound) {
				bindService(intent, serviceCon, Context.BIND_AUTO_CREATE);
				accel_detect_starbtn.setEnabled(isBound);
				isBound = true;
				accel_detect_stopbtn.setEnabled(isBound);
			}

		}
	};

	OnClickListener laccel_detect_servicestop_listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (isBound) {
				accel_detect_starbtn.setEnabled(isBound);
				isBound = false;
				accel_detect_stopbtn.setEnabled(isBound);

				StringBuilder Y_value = new StringBuilder();
				for (int num = 0; num < sensorData.size(); num++) {
					Y_value.append(sensorData.get(num).getY() + " ,\t");
					accel_text.setText(Y_value);
				}
				unbindService(serviceCon);
				Log.d("accel value", Y_value.toString());
			}
		}

	};

	OnClickListener training_data_collect_listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent it = new Intent(getBaseContext(), TrainingDataCollect.class);
			startActivity(it);
		}
	};

	OnClickListener fall_detection_listener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			Intent it = new Intent(getBaseContext(), FallDetection.class);
			startActivity(it);
		}
	};
	OnClickListener audio_setting_listener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			Intent it = new Intent(getBaseContext(), AudioSetting.class);
			startActivity(it);
		}
	};
	OnClickListener callphone_setting_listener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			Intent it = new Intent(getBaseContext(), ActivityPhoneCall.class);
			startActivity(it);
		}
	};

	OnClickListener sms_setting_listener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			Intent it = new Intent(getBaseContext(), SmsSettingActivity.class);
			startActivity(it);
		}
	};

	OnClickListener startgps_listener = new OnClickListener() {

		@Override
		public void onClick(View v) {

			Intent localIntent = new Intent(
					"android.settings.LOCATION_SOURCE_SETTINGS");
			startActivity(localIntent);
		}
	};

	@Override
	protected void onResume() {
		super.onResume();
		checkGPSOption();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private ServiceConnection serviceCon = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			sensorData = ((AccelDetectService.ServiceBinder) service)
					.getSensorData();
			Toast.makeText(MainActivity.this, "服務啟動成功 ", Toast.LENGTH_LONG)
					.show();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Toast.makeText(MainActivity.this, "啟動服務失敗", Toast.LENGTH_LONG)
					.show();
		}
	};

}
