package edu.mosense;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.RingtonePreference;

public class AudioSetting extends AbstractBasePreferencesActivity implements OnPreferenceChangeListener
{
	 RingtonePreference mAlarmPref ;
	 Preference issueAlertPreference;
	 EditTextPreference editTextSecondPreference;
	// ListPreference  listSecondPreference;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getPreferenceManager().setSharedPreferencesName("audiosetting");
		addPreferencesFromResource(R.xml.preference_setting);
		
	    mAlarmPref = (RingtonePreference) findPreference("ringtone");
		mAlarmPref.setOnPreferenceChangeListener(this);
		
		editTextSecondPreference = (EditTextPreference) findPreference("Timeout");
		editTextSecondPreference.setOnPreferenceChangeListener(this);
		
//		listSecondPreference=(ListPreference) findPreference("listPref");
//		listSecondPreference.setOnPreferenceChangeListener(this);

		issueAlertPreference = findPreference("issue_alert");
        SharedPreferences sharedPreferences= issueAlertPreference.getSharedPreferences();

		if (sharedPreferences.getBoolean("issue_alert", false)){
			mAlarmPref.setEnabled(true);
			editTextSecondPreference.setEnabled(true);
			//listSecondPreference.setEnabled(true);
		}
		else{
			mAlarmPref.setEnabled(false);
			editTextSecondPreference.setEnabled(false);
			//listSecondPreference.setEnabled(false);
		}
		//issueAlertPreference.setOnPreferenceChangeListener(this);
      
  

	}
	   @Override
	    protected void onResume() {
	        super.onResume();

	        // Setup the initial values
			//listSecondPreference.setSummary("目前選擇的秒數: " + listSecondPreference.getEntry().toString());

	        // Set up a listener whenever a key changes            
	        //getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener((OnSharedPreferenceChangeListener) this);
	    }
	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue)
	{
		if(preference.equals(mAlarmPref)){
//		preference.setSummary(String.valueOf(newValue));		
		mAlarmPref.setSummary(getRingtonName(Uri.parse(newValue.toString())));
        SharedPreferences sp=mAlarmPref.getPreferenceManager().getSharedPreferences();  
        sp.edit().putString("ringtone", newValue.toString()).commit();  
		}
		
		if(preference.equals(editTextSecondPreference)){
	        SharedPreferences sp=editTextSecondPreference.getPreferenceManager().getSharedPreferences();  
	        sp.edit().putString("Timeout",  String.valueOf(newValue)).commit();  
			}
		
//		if(preference.equals(listSecondPreference)){
////			preference.setSummary(String.valueOf(newValue));		
//			listSecondPreference.setSummary("目前選擇的秒數: " + listSecondPreference.getEntry().toString());
//	        SharedPreferences sp=listSecondPreference.getPreferenceManager().getSharedPreferences();  
//	        sp.edit().putString("listPref",  String.valueOf(newValue)).commit();  
//			}
		
        return false;
	}

	public String getRingtonName(Uri uri)
    {
        Ringtone r=RingtoneManager.getRingtone(this, uri);
        return r.getTitle(this);
    }
	
	@Override
	public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen,Preference preference)
	{

		if ("issue_alert".equals(preference.getKey()))
		{
			mAlarmPref.setEnabled(!mAlarmPref.isEnabled());
			editTextSecondPreference.setEnabled(!editTextSecondPreference.isEnabled());
//			listSecondPreference.setEnabled(!listSecondPreference.isEnabled());

		}
		return super.onPreferenceTreeClick(preferenceScreen, preference);
	}
}
