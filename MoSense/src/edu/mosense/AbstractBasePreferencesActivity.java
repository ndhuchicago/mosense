package edu.mosense;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.app.SherlockPreferenceActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

public class AbstractBasePreferencesActivity extends SherlockPreferenceActivity {
	private TextView txtActionBarTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActionBar bar = getSupportActionBar();
		bar.setDisplayHomeAsUpEnabled(true);
		bar.setDisplayShowCustomEnabled(true);
		bar.setLogo(R.drawable.ic_action_main);
		bar.setCustomView(R.layout.action_bar);

		txtActionBarTitle = getTxtActionBarTitle();
		setTitle(getTitle());
	}

	@Override
	public void setTitle(CharSequence title) {
		super.setTitle(title);
		getTxtActionBarTitle().setText(title);
	}

	@Override
	public void setTitle(int titleId) {
		super.setTitle(titleId);
		setTitle(getResources().getString(titleId));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getSupportMenuInflater();
		inflater.inflate(R.menu.base, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Class<?> cls = null;

		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent = new Intent(this, MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		case R.id.liTrainingDataCollect:
			cls = TrainingDataCollect.class;
			break;
		case R.id.liFallDetection:
			cls = FallDetection.class;
			break;
		case R.id.liAlertSetting:
			cls = AudioSetting.class;
			break;
		case R.id.liCallphoneSetting:
			cls = ActivityPhoneCall.class;
			break;
		case  R.id.liSms:
			cls = SmsSettingActivity.class;
			break;
		}

		if (cls != null && !cls.equals(getClass())) {
			Intent intent = new Intent(this, cls);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			//startActivity(new Intent(this, cls));
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	public Activity getActivity() {
		return this;
	}

	public Context getContext() {
		return this;
	}

	private TextView getTxtActionBarTitle() {
		return (txtActionBarTitle == null) ? (TextView) findViewById(R.id.txtActionBarTitle)
				: txtActionBarTitle;
	}
}