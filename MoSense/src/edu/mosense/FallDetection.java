package edu.mosense;

import java.util.ArrayList;
import edu.mosense.service.*;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
//import android.renderscript.Mesh.TriangleMeshBuilder;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class FallDetection extends AbstractBaseActivity {
	private static final int NOTI_ID = 100;
	private static ImageView mFallStatusImage;
	private SharedPreferences mSharedPreferences;
	 private StateChangeReceiver mStateChangeReceiver;
	private Button starbtn;
	private Button stopbtn;
	private TextView accel_text;
	private EditText  falltimetxt ,squattimetxt ,sittimetxt ,jumptimetxt,othertxt;
	private int fallcount=0,othercount=0;
	public ArrayList<AccelData> sensorData;
	public ArrayList<AccelData> RawSensorData;
	public ArrayList<AccelData> NormalizationSensorData;
	private Intent accelDetectServiceIntent ;
	private int Firstnum, Endnum;
	private boolean isBound = false;
	private String charSymbol;
	private String[] abnmlLcsTrain = new String[4];
	private String[] abnmlLcsTrain2 = new String[4];
	
	private String[] trainData=new String[4];
	private String[] trainData2=new String[4];
	private int[] trainDifNum = new int[4];
	private int[] trainDifNum2 = new int[4];
	private int[] trainDeviSum = new int[4];
	private int[] trainDeviSum2 = new int[4];
	private double[] trainDataScore = new double[4];
	private double[] trainDataScore2 = new double[4];
	private double[] trainAvgLocation = new double[4];
	private double[] trainAvgLocation2 = new double[4];

	private SimilarCalculate[]  similarCalculate = new SimilarCalculate[4];
	private SimilarCalculateMerge[]  similarCalculateMerge = new SimilarCalculateMerge[4];

	private double[] similarValue = new double[4];
	private double[] similarValueMerge = new double[4];
	private int[] deviSumDif = new int[4];
	private double[] rotationAngle,rotationAnglePitch,rotationAngleRoll;
	private double angleVariation ,pitchAngleVariation,rollAngleVariation;
	
	private ApproxLCS trainXcharSymbolData,trainXcharSymbolData2 ;
	private RotationAngle rotationAngleObject;
	private int PositionSumSubAvgLocation,PositionSumSubAvgLocation2;
	private int PositionSum,PositionSum2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {		 
		/**變數初始化*/
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fallone);
		mFallStatusImage = (ImageView)findViewById(R.id.img_fall_status);
	    //mStateChangeReceiver = new StateChangeReceiver();
		mSharedPreferences = getSharedPreferences("audiosetting",Activity.MODE_PRIVATE);
		registerReceiver(AbnmlProcsBroRec, new IntentFilter("edu.mosense.service.AbnmlProcsBroRec"));
		accelDetectServiceIntent = new Intent(FallDetection.this,edu.mosense.service.AccelDetectService.class);
		trainData[0]="LLLBDONLNBCC";
		trainData2[0]="LLLACBNMLBA";
		trainDifNum[0]=6;
		trainDifNum2[0]=6;
		trainDeviSum[0]=41;
		trainDeviSum[02]=39;
		trainDataScore[0]=873;
		trainDataScore2[0]=788;
		trainAvgLocation[0]=17;
		trainAvgLocation2[0]=17;

		/** 元件初始化 */
		starbtn = (Button) findViewById(R.id.DFstarbtn);
		stopbtn = (Button) findViewById(R.id.DFstopbtn);
		accel_text = (TextView) findViewById(R.id.textView2);
		accel_text.setMovementMethod(ScrollingMovementMethod.getInstance()); 
		falltimetxt = (EditText)findViewById(R.id.falltime);
		othertxt = (EditText)findViewById(R.id.other);
		
		/** 設定監聽器 */
		starbtn.setOnClickListener(star_listener);
		stopbtn.setOnClickListener(stop_listener);
		stopbtn.setEnabled(isBound);
	}

	/** 監聽事件 */
	OnClickListener star_listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
		if (!isBound) {
				boolean ServiceCheck = bindService(accelDetectServiceIntent, serviceCon, Context.BIND_AUTO_CREATE);
				//startService(new Intent(getBaseContext(), AccelDetectService.class));
				showNotification();
//				Intent localIntent = new Intent("ACTION_STATECHANGE");
//			    localIntent.putExtra("currentstate", 1);
//			    getBaseContext().sendBroadcast(localIntent);
				//	if (ServiceCheck) {
					starbtn.setEnabled(isBound);
					isBound = true;
					stopbtn.setEnabled(isBound);
					falltimetxt.setText("0");
					othertxt.setText("0");
					Toast.makeText(FallDetection.this, "偵測啟動成功 ", Toast.LENGTH_SHORT).show();
			//	}else{
			//		Toast.makeText(FallDetection.this, "偵測啟動失敗 ", Toast.LENGTH_SHORT).show();
			//	}
			}
		}
	};

	OnClickListener stop_listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if (isBound) {
				starbtn.setEnabled(isBound);
				isBound = false;
				stopbtn.setEnabled(isBound);
				//stopService(new Intent(getBaseContext(), AccelDetectService.class));
				NotificationManager notiMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
			    notiMgr.cancel(NOTI_ID);
//				Intent localIntent = new Intent("ACTION_STATECHANGE");
//			    localIntent.putExtra("currentstate", 0);
//			    getBaseContext().sendBroadcast(localIntent);
				unbindService(serviceCon);
				Toast.makeText(FallDetection.this, "偵測停止 ", Toast.LENGTH_SHORT).show();
			}
			fallcount=0;
			othercount=0;
		}
	};

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
	   // unregisterReceiver(this.mStateChangeReceiver);

	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResume() {
		super.onResume();
//		 registerReceiver(mStateChangeReceiver, new IntentFilter("ACTION_STATECHANGE"));
		 SharedPreferences OnOffPerferences = getSharedPreferences("OnOffstate", Activity.MODE_PRIVATE);
//			if(OnOffPerferences.getBoolean("is_monitoring", false)){
//				isBound=true;
//				starbtn.setEnabled(false);
//		    	stopbtn.setEnabled(true);
////				Intent localIntent = new Intent("ACTION_STATECHANGE");
////			    localIntent.putExtra("currentstate", 1);
////			    getBaseContext().sendBroadcast(localIntent);
//		    	}
//			else{
//				isBound=false;
//				starbtn.setEnabled(true);
//				stopbtn.setEnabled(false);				
////				Intent localIntent = new Intent("ACTION_STATECHANGE");
////			    localIntent.putExtra("currentstate", 0);
////			    getBaseContext().sendBroadcast(localIntent);
//			    }
				
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	private ServiceConnection serviceCon = new ServiceConnection() {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			sensorData = ((AccelDetectService.ServiceBinder) service).getSensorData();
			RawSensorData = ((AccelDetectService.ServiceBinder) service).getSensorData();
			//Toast.makeText(FallDecOne.this, "服務啟動成功 ", Toast.LENGTH_LONG).show();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Toast.makeText(FallDetection.this, "啟動服務失敗", Toast.LENGTH_LONG).show();
		}
	};

	private BroadcastReceiver AbnmlProcsBroRec = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if ("edu.mosense.service.AbnmlProcsBroRec".equals(intent.getAction())) {				
				StringBuilder Y_value = new StringBuilder();
//				Firstnum = abnmlIndex.getFirstIndex();
//				Endnum = abnmlIndex.getEndIndex();
//				int EndFirst = abnmlIndex.getEndIndex() - abnmlIndex.getFirstIndex() + 1;
//				for (int i = 0; i < EndFirst; i++) {
//					Y_value.append(sensorData.get(Firstnum + i).getY() + " ,\t");
//					//accel_text.setText(Y_value);
//				}
				unbindService(serviceCon);
				//stopService(new Intent(getBaseContext(), AccelDetectService.class));
				//sensorData = (ArrayList<AccelData>) intent.getSerializableExtra("sensorData");
				//RawSensorData =(ArrayList<AccelData>) intent.getSerializableExtra("sensorDataRaw");
				
				rotationAngleObject = new RotationAngle(RawSensorData);
				rotationAngle = rotationAngleObject.getRotationAngle();
				angleVariation =Math.ceil(rotationAngleObject.getAngleVariation());
				rotationAnglePitch = rotationAngleObject.getRotationAnglePitch();
				rotationAngleRoll = rotationAngleObject.getRotationAngleRoll();
				pitchAngleVariation = Math.ceil(rotationAngleObject.getPitchAngleVariation());
				rollAngleVariation = Math.ceil(rotationAngleObject.getRollAngleVariation());
				Y_value.append("角度變化量(原始,Roll):"+angleVariation+","+rollAngleVariation+"\n"+"異常偵測資料角度"+"("+rotationAngle.length+"):"+"\n");
//				for(int i = 0 ; i< rotationAngle.length ; i++){
//					//Y_value.append(rotationAngle[i]+", ");
//					Y_value.append("("+rotationAngle[i]+" : "+rotationAngleRoll[i]+")"+", ");
//				}
				//Y_value.append("\n");
//				Toast.makeText(FallDecOne.this, "服務成功停止", Toast.LENGTH_LONG).show();
				NormalizationSensorData=new Normalization(sensorData).getAcceldatas();
				//new PearsonCorrelationCoefficient(NormalizationSensorData).getRelationValue();
				charSymbol =  removeChr(new FeatureExtra(NormalizationSensorData).getCharSymbol(),'X');
//				charSymbol = new FeatureExtra(sensorData,Firstnum,Endnum, 0.1).getCharSymbol();				
				Y_value.append("原始異常偵測資料:"+new FeatureExtra(NormalizationSensorData).getCharSymbol()+"\n"+"異常偵測資料:"+charSymbol+"\n"+"異常偵測資料長度:"+charSymbol.length()+"\n"+"跌倒訓練資料:"+trainData[0]+"\n"+"跌倒訓練資料2:"+trainData2[0]);	
				
				trainXcharSymbolData = new ApproxLCS(trainData[0],charSymbol );
				trainXcharSymbolData2 = new ApproxLCS(trainData2[0],charSymbol );
				abnmlLcsTrain[0] = trainXcharSymbolData.getLcs();
				abnmlLcsTrain2[0] = trainXcharSymbolData2.getLcs();
				Y_value.append("\n跌倒結果:"+abnmlLcsTrain[0]+"\n跌倒結果2:"+abnmlLcsTrain2[0]);	
				PositionSum = trainXcharSymbolData.getPositionSum();
				PositionSum2 = trainXcharSymbolData2.getPositionSum();
				PositionSumSubAvgLocation =(int) Math.abs(PositionSum-trainAvgLocation[0]);
				PositionSumSubAvgLocation2 =(int) Math.abs(PositionSum2-trainAvgLocation2[0]);
				Y_value.append("\n"+"PositionSum:"+PositionSum+"\t"+"trainAvgLocation:"+trainAvgLocation[0]+"\t"+"PositionSumSubAvgLocation:"+PositionSumSubAvgLocation);		
				Y_value.append("\n"+"PositionSum2:"+PositionSum2+"\t"+"trainAvgLocation2:"+trainAvgLocation2[0]+"\t"+"PositionSumSubAvgLocation2:"+PositionSumSubAvgLocation2);		

				/**舊版相似度計算
				similarCalculate[0] =  new SimilarCalculate(abnmlLcsTrain[0], trainData[0].length(), trainDeviSum[0],trainDifNum[0], trainDataScore[0]);
				similarCalculate[1] =  new SimilarCalculate(abnmlLcsTrain2[0], trainData2[0].length(), trainDeviSum2[0],trainDifNum2[0], trainDataScore2[0]);
				similarValue[0] = Math.ceil(similarCalculate[0].getSimilarValue());
				similarValue[1] = Math.ceil(similarCalculate[1].getSimilarValue());
				deviSumDif[0] = similarCalculate[0].getDeviSumDif();
				deviSumDif[1] = similarCalculate[1].getDeviSumDif();
				Y_value.append("\n與跌倒相似度:"+similarValue[0]+"\n與跌倒離差總合差:"+deviSumDif[0]);
				Y_value.append("\n與跌倒相似度2:"+similarValue[1]+"\n與跌倒離差總合差2:"+deviSumDif[1]);
				 */
				similarCalculateMerge[0] =  new SimilarCalculateMerge(abnmlLcsTrain[0],trainData[0] ,trainData[0].length(), trainDeviSum[0],trainDifNum[0], trainDataScore[0]);
				similarValueMerge[0]= Math.ceil(similarCalculateMerge[0].getSimilarMergeValue());
				deviSumDif[0] = similarCalculateMerge[0].getDeviSumDif();
				Y_value.append("\n與跌倒相似度:"+similarValueMerge[0]+"\n與跌倒離差總合差:"+deviSumDif[0]+"\n模糊LCS分數:"+similarCalculateMerge[0].getApproxLCSScore()+"\n模糊LD分數:"+similarCalculateMerge[0].getApproxLDScore());

				if(PositionSumSubAvgLocation <= 10){
					
					if(similarValueMerge[0]>=70 ){
						fallcount = fallcount+1;
						falltimetxt.setText( Integer.toString(fallcount));
						if(mSharedPreferences.getBoolean("issue_alert", false)){
							Intent it = new Intent();  
							it.setClass(getBaseContext(), AlertActivity.class);
							it.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
							startActivity(it);
						}
					}else{
//						if (similarValue[0]>=60 || similarValue[1] >= 60) {
//							fallcount = fallcount+1;
//							falltimetxt.setText( Integer.toString(fallcount));
//						} else {
							othercount=othercount+1;
							othertxt.setText(Integer.toString(othercount));
//						}			
					}
					accel_text.setText(Y_value);
					bindService(accelDetectServiceIntent, serviceCon, Context.BIND_AUTO_CREATE);
					//startService(new Intent(getBaseContext(), AccelDetectService.class));
				}else {

					accel_text.setText(Y_value);
					bindService(accelDetectServiceIntent, serviceCon, Context.BIND_AUTO_CREATE);
					//startService(new Intent(getBaseContext(), AccelDetectService.class));
				}
				
//				int a = findMaxValueLocation(similarValue);
//				Y_value.append("\n相似度最大項索引:"+Integer.toString(a));
//				accel_text.setText(Y_value);
//				switch(a){
//				case 0:
//					if (similarValue[0]>=80 ) {
//						fallcount=fallcount+1;
//						falltimetxt.setText(Integer.toString(fallcount));
//						break;
//					}else{
//						othercount=othercount+1;
//						othertxt.setText(Integer.toString(othercount));
//						break;
//					}
//				case 1:
//					if (similarValue[1]>=80 ) {
//						squatcount = squatcount+1;
//						squattimetxt.setText(Integer.toString(squatcount));
//						break;
//					}else{
//						othercount=othercount+1;
//						othertxt.setText(Integer.toString(othercount));
//						break;
//					}
//				case 2:
//					if (similarValue[2]>=80 ) {
//						sitcount = sitcount+1;
//						sittimetxt.setText(Integer.toString(sitcount));
//						break;
//					}else{
//						othercount=othercount+1;
//						othertxt.setText(Integer.toString(othercount));
//						break;
//					}
//				case 3:
//					if (similarValue[3]>=80 ) {
//						jumpcount = jumpcount+1;
//						jumptimetxt.setText(Integer.toString(jumpcount));
//						break;
//					}else{
//						othercount=othercount+1;
//						othertxt.setText(Integer.toString(othercount));
//						break;
//					}
//					default:
//						break;
//				}
				
//				if(similarValue[0]>=75 ){
//					fallcount = fallcount+1;
//					falltimetxt.setText( Integer.toString(fallcount));
//				}else{
//					if (similarValue[0]>=60 && deviSumDif[0]<=10) {
//						fallcount = fallcount+1;
//						falltimetxt.setText( Integer.toString(fallcount));
//					} else {
//						othercount=othercount+1;
//						othertxt.setText(Integer.toString(othercount));
//					}			
//				}
//				accel_text.setText(Y_value);
//				bindService(accelDetectServiceIntent, serviceCon, Context.BIND_AUTO_CREATE);
			}

		}
	private int findMaxValueLocation(double[] similarValue){
		int i, n2 = 0;
		double max;
		double num[] = similarValue;
		
		max = num[0];
			for (i = 0; i < num.length; i++) {	
				if (num[i] > max) {
					n2 = i;
					max = num[i];
				}
			}			
			return n2;
		}
	
	private String  removeChr(String str, char x) {
		StringBuilder strBuilder = new StringBuilder();
		char[] rmString = str.toCharArray();
		for (int i = 0; i < rmString.length; i++) {
			if (rmString[i] == x) {

			} else {
				strBuilder.append(rmString[i]);
			}
		}
		return strBuilder.toString();
	}
	
	};
	private void showNotification() {
	    Notification noti = new Notification(R.drawable.on_notification,"iFall Aware is ON",System.currentTimeMillis());
	    Intent it = new Intent();
	    it.setClass(this, FallDetection.class);
	    it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	 	    PendingIntent penIt = PendingIntent.getActivity( this, 0, it, PendingIntent.FLAG_UPDATE_CURRENT);
	    noti.setLatestEventInfo(this, "異常動作偵測", "加速度感測器監測中...", penIt);			
	    NotificationManager notiMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	    notiMgr.cancel(NOTI_ID);
	    notiMgr.notify(NOTI_ID, noti);
	}
	private class StateChangeReceiver extends BroadcastReceiver
	  {
	    public static final int OFF = 0;
	    public static final int ON = 1;
	    public static final int RISKFALL = 2;
	    int mState;

	    private StateChangeReceiver()
	    {
	    }

	    public void onReceive(Context paramContext, Intent paramIntent)
	    {
	      this.mState = paramIntent.getIntExtra("currentstate", -1);
	      switch (this.mState)
	      {
	      default:
	        return;
	      case 2:
	    	  mFallStatusImage.setImageResource(R.drawable.androidrisk);
	        return;
	      case 1:
	    	  mFallStatusImage.setImageResource(R.drawable.android_on);
	        return;
	      case 0:
	    	  mFallStatusImage.setImageResource(R.drawable.android_off);
	      }
	      //MonitoringActivity.mFallStatusImage.setImageResource(2130837512);
	    }
	  }
}